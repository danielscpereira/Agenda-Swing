/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Estado;

/**
 *
 * @author Administrador
 */
public class EstadoDAO implements DAO<Estado> {
    
    @Override
    public List<Estado> findAll() throws Exception {
       
        List<Estado> listaEstados = new ArrayList<>() ; 
        
        Connection connection = Conexao.getConnection() ; 
        String sql = "Select * from Estado";
        
        Statement statement = connection.createStatement() ; 
        
        ResultSet rs = statement.executeQuery(sql) ; 
        
        while (rs.next()) {            
            
            Estado estado = new Estado() ; 
            estado.setId(rs.getInt("Id"));
            estado.setNome(rs.getString("nome"));
            estado.setSigla(rs.getString("sigla"));
            listaEstados.add(estado);
            
        }
        
        return listaEstados;
        
    }
    
    @Override
    public Estado getById(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void save(Estado objeto) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void delete(Estado objeto) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
