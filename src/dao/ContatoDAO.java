/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Contato;
import model.Endereco;
import model.Estado;

/**
 *
 * @author Administrador
 */
public class ContatoDAO implements DAO<Contato> {

    @Override
    public List<Contato> findAll() throws Exception {
        List<Contato> listaContato = new ArrayList<>();

        Connection connection = null;
        try {

            connection = Conexao.getConnection();
            String sql = "SELECT  "
                    + "		C.id, C.nome ,  "
                    + "        C.telefoneResidencial ,  "
                    + "        C.telefoneCelular ,  "
                    + "        C.cep ,"
                    + "        C.logradouro ,  "
                    + "        C.bairro ,  "
                    + "        C.cidade ,  "
                    + "        C.numero ,  "
                    + "        C.idEstado ,  "
                    + "        E.nome AS nomeEstado ,  "
                    + "        E.sigla "
                    + "FROM CONTATO C  "
                    + "INNER JOIN ESTADO E  "
                    + "ON C.idEstado = E.id ";

            Statement statement = connection.createStatement();

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {

                Estado estado = new Estado();
                estado.setId(rs.getInt("idEstado"));
                estado.setNome(rs.getString("nomeEstado"));
                estado.setSigla(rs.getString("sigla"));

                Contato contato = new Contato();
                contato.setId(rs.getInt("id"));
                contato.setNome(rs.getString("nome"));
                contato.setTelefoneResidencial(rs.getString("telefoneResidencial"));
                contato.setTelefoneCelular(rs.getString("telefoneCelular"));
                contato.getEndereco().setCep(rs.getLong("cep"));
                contato.getEndereco().setLogradouro(rs.getString("logradouro"));
                contato.getEndereco().setBairro(rs.getString("bairro"));
                contato.getEndereco().setCidade(rs.getString("cidade"));
                contato.getEndereco().setNumero(rs.getInt("numero"));
                contato.getEndereco().setEstado(estado);

                listaContato.add(contato);

            }

        } catch (Exception ex) {
            throw new Exception("Erro ao realizar pesquisa.\n" + ex.getMessage());

        } finally {
            try {
                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return listaContato;
    }

    public List<Contato> findByNome(String nome) throws Exception {
        List<Contato> listaContato = new ArrayList<>();

        Connection connection = null;
        try {
            connection = Conexao.getConnection();
            String sql = "SELECT  "
                    + "		C.id, C.nome ,  "
                    + "        C.telefoneResidencial ,  "
                    + "        C.telefoneCelular ,  "
                    + "        C.cep ,"
                    + "        C.logradouro ,  "
                    + "        C.bairro ,  "
                    + "        C.cidade ,  "
                    + "        C.numero ,  "
                    + "        C.idEstado ,  "
                    + "        E.nome AS nomeEstado ,  "
                    + "        E.sigla "
                    + "FROM CONTATO C  "
                    + "INNER JOIN ESTADO E  "
                    + "ON C.idEstado = E.id "
                    + "where 1 = 1 ";

            if (!nome.isEmpty()) {
                sql += " and c.nome like ?";
            }

            PreparedStatement ps = connection.prepareStatement(sql);

            if (!nome.isEmpty()) {
                ps.setString(1, nome + "%");

            }

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                Estado estado = new Estado();
                estado.setId(rs.getInt("idEstado"));
                estado.setNome(rs.getString("nomeEstado"));
                estado.setSigla(rs.getString("sigla"));

                Contato contato = new Contato();
                contato.setId(rs.getInt("id"));
                contato.setNome(rs.getString("nome"));
                contato.setTelefoneResidencial(rs.getString("telefoneResidencial"));
                contato.setTelefoneCelular(rs.getString("telefoneCelular"));
                contato.getEndereco().setCep(rs.getLong("cep"));
                contato.getEndereco().setLogradouro(rs.getString("logradouro"));
                contato.getEndereco().setBairro(rs.getString("bairro"));
                contato.getEndereco().setCidade(rs.getString("cidade"));
                contato.getEndereco().setNumero(rs.getInt("numero"));
                contato.getEndereco().setEstado(estado);

                listaContato.add(contato);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("Erro ao realizar pesquisa.\n" + ex.getMessage());

        } finally {
            try {
                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return listaContato;
    }

    @Override
    public Contato getById(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void save(Contato contato) throws Exception {

        Connection connection = null;
        try {
            connection = Conexao.getConnection();
            String sql = null;

            if (contato.getId() == 0) {
                sql = "INSERT INTO contato  "
                        + "( nome ,  telefoneResidencial ,  telefoneCelular ,  cep ,  logradouro ,  bairro ,  cidade ,  numero ,  idEstado ) "
                        + "VALUES (?,?,?,?,?,?,?,?,?) ; ";
            } else {
                sql = "UPDATE  contato  "
                        + "SET  "
                        + " nome  ? , "
                        + " telefoneResidencial  = ? , "
                        + " telefoneCelular  = ? , "
                        + " cep  = ? , "
                        + " logradouro  = ? , "
                        + " bairro  = ? , "
                        + " cidade  = ? , "
                        + " numero  = ? , "
                        + " idEstado  = ?  "
                        + " WHERE  id  = ? ; ";
            }

            PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, contato.getNome());
            ps.setString(2, contato.getTelefoneResidencial());
            ps.setString(3, contato.getTelefoneCelular());
            ps.setLong(4, contato.getEndereco().getCep());
            ps.setString(5, contato.getEndereco().getLogradouro());
            ps.setString(6, contato.getEndereco().getBairro());
            ps.setString(7, contato.getEndereco().getCidade());
            ps.setInt(8, contato.getEndereco().getNumero());
            ps.setInt(9, contato.getEndereco().getEstado().getId());
            if (contato.getId() != 0) {
                ps.setInt(10, contato.getId());
            }

            ps.executeUpdate();

            if (contato.getId() == 0) {
                ResultSet rs = ps.getGeneratedKeys();
                rs.first();
                contato.setId(rs.getInt(1));
            }

        } catch (Exception ex) {
            throw new Exception("Erro ao salvar.\n" + ex.getMessage());

        } finally {
            try {
                connection.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public void delete(Contato contato) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
