package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Conexao {

    /*Configuração do banco */
    //private final static String URL = "jdbc:mysql//localhost:3306/meubanco"; -->URL Final
    private final static String MAQUINA = "localhost";
    private final static String USUARIO = "root";
    private final static String SENHA = "123456";
    private final static String PORTA = "3306";
    private final static String BANCO = "agenda";

    private final static String DRIVER = "com.mysql.jdbc.Driver";

    public static Connection getConnection() {
        //jdbc:mysql://localhost:3306/eleicoes2016
        String url = "jdbc:mysql://" + MAQUINA + ":" + PORTA + "/" + BANCO;   //jdbc:mysql//localhost:3306/eleicoes2016

        System.out.println(url);

        Connection connection = null;

        try {

            System.out.println("Tentando carregar driver do banco..");
            Class.forName(DRIVER); // Carregar driver do banco !
            System.out.println("Driver carregado com sucesso ..");

            System.out.println("Tentando conectar com o  banco..");
            connection = DriverManager.getConnection(url, USUARIO, SENHA);
            System.out.println("Conectado com sucesso  banco..");

        } catch (ClassNotFoundException ex) {
            System.out.println("Erro ao carregar driver do banco !");
        } catch (SQLException ex) {
            System.out.println("Falha ao conectar ao banco !");
            ex.printStackTrace();
        }

        return connection;
    }


}
