package dao;

import java.util.List;

public interface DAO<T> {

    List<T> findAll()throws Exception;

    T getById(int id)throws Exception;

    void save(T objeto) throws Exception;

    void delete(T objeto)throws Exception;

}
