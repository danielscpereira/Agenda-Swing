
package exception;


public class FormularioValidationException extends Exception{

    public FormularioValidationException() {
        super("Existem campos obrigatórios não preenchidos");
    }
}
